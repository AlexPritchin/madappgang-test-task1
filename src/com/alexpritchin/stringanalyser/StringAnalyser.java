package com.alexpritchin.stringanalyser;

public class StringAnalyser {

    private static final String oneSpaceAfterChars = ".,!?:";
    private static final String noSpaceChars = "'�";
    private static final String upCaseAfterChars = ".!?";

    static String repairString(String inputStr) {
        StringBuilder workStr = new StringBuilder(inputStr.trim());
        boolean isEndOfSentence = true;

        // Check letters and change their case if needed.
        for (int i = 0; i < workStr.length(); i++) {
            if (upCaseAfterChars.indexOf(workStr.charAt(i)) > -1) {
                isEndOfSentence = true;
                continue;
            }
            if (isEndOfSentence) {
                if (Character.isLowerCase(workStr.charAt(i)))
                    workStr.setCharAt(i, Character.toUpperCase(workStr.charAt(i)));
                if (Character.isLetter(workStr.charAt(i)))
                    isEndOfSentence = false;
            }
            else
                if (Character.isUpperCase(workStr.charAt(i)))
                    workStr.setCharAt(i, Character.toLowerCase(workStr.charAt(i)));
        }

        // Delete invalid spaces
        int spaceNum = 0;
        while ((spaceNum = workStr.indexOf(" ", spaceNum)) > -1) {
            if ((oneSpaceAfterChars.indexOf(workStr.charAt(spaceNum + 1)) > -1) ||
                    (noSpaceChars.indexOf(workStr.charAt(spaceNum + 1)) > -1) ||
                    (noSpaceChars.indexOf(workStr.charAt(spaceNum - 1)) > -1))
                workStr.deleteCharAt(spaceNum);
            spaceNum += 1;
        }

        // Add valid spaces
        for (int i = 1; i < workStr.length(); i++)
            if (Character.isLetter(workStr.charAt(i)))
                if ((oneSpaceAfterChars.indexOf(workStr.charAt(i - 1)) > -1))
                    workStr.insert(i, ' ');

        return workStr.toString();
    }

}
