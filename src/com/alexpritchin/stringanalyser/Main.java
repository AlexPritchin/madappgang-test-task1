package com.alexpritchin.stringanalyser;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    System.out.println("String Analyser 1.0");
        System.out.print("Enter the string to repair: ");

        Scanner screenScan = new Scanner(System.in);
        String userStr = screenScan.nextLine();

        System.out.println("Your string:");
        System.out.println(userStr);

        String repairedStr = StringAnalyser.repairString(userStr);

        System.out.println("Repaired string:");
        System.out.println(repairedStr);
    }
}
